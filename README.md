# Datamanager

A simple data manager that can be used to read, save or download files (local or remote) in a local database. The purpose of this package is to provide a simple and secure way to manage files and databases in a local environment treating them as defined objects to facilitate their interaction.

- [Datamanager](#datamanager)
  - [What is a file here?](#what-is-a-file-here)
  - [What is a database directory here?](#what-is-a-database-directory-here)
  - [Registry file, what for?](#registry-file-what-for)
  - [Working with a local database](#working-with-a-local-database)
    - [Without a registry file](#without-a-registry-file)
    - [With a registry file](#with-a-registry-file)
  - [Working without a local database](#working-without-a-local-database)
  - [Database manager](#database-manager)
  - [File manager](#file-manager)
  - [Installation](#installation)
  - [Usage](#usage)

## What is a file here?
Here, a file is another layer of abstraction concept and resides on-top of the OS file system host abstraction. From here on, we called it as `FILE` (all in uppercase).

A `FILE` has a filename with a specific format that it is well known and is defined somewhere.

A common situation could be:

```html
<prefix><id-sep><id><id-sep><suffix><field-sep><fields><ext-sep><extensions>
```

Or a simpler case could be:

```html
<id><ext-sep><extensions>
```

As it can be seen, the **filename** is composed of the following sections:

| Section | Relevance | Description |
|---------|-----------|-----------|
| Primary | | |
| `<prefix>` | optional | Is a string that can be used to identify the file |
| `<suffix>` | optional | Is a string that can be used to identify the file |
| `<id>` | mandatory | Is a unique identifier of the file |
| Secondary | | |
| `<fields>` | optional | Is a list of field descriptors |
| `<extensions>` | mandatory | Is a list of extensions |
| Separators | | |
| `<id-sep>` | mandatory | Is a separator of the id |
| `<field-sep>` | optional | Is a separator of the fields |
| `<ext-sep>` | mandatory | Is a separator of the extensions |

## What is a database directory here?
Here, a dabase directory is another layer of abstraction concept and resides on-top of the OS file system host abstraction. From here on, we called it as `DB_DIR` (all in uppercase).

A `DB_DIR` has a directory structure that is well known and is defined somewhere.

A common situation could be:

```html
<root><internal_node><terminal_node>
```

As it can be seen, the **directory structure** is composed of the following sections:

| Section | Relevance | Description |
|---------|-----------|-----------|
| Root | | |
| `<root>` | mandatory | Is the root directory of the database |
| Internal | | |
| `<internal_node>` | optional | Is a internal node of the database |
| Terminal | | |
| `<terminal_node>` | mandatory | Is a terminal node of the database |


## Registry file, what for?
A registry file is a compressed json file that stores the information of files using records. All records are unique to its registry file and can be find it by its creation time (a date). The structure of any registry file is already defined as the following format.

```json
{
    "location"          : str, # Absolute path to the registry file
    "database_dir"      : str, # Absolute path to the database directory
    "creation_time"     : str, # YYYY-MM-DD HH:MM:SS, creation time of the registry file
    "modification_time" : str, # YYYY-MM-DD HH:MM:SS, last modification time of the registry file
    "records"           : [ {
            "creation_time"     : str, # YYYY-MM-DD HH:MM:SS, creation time of the record
            "nb_files"          : int, # Number of files in the record
            "files"             : [ {
                "url"               : str,  # URL to the file
                "filepath"          : str,  # Absolute path to the file
                "md5_signature"     : str,  # MD5 hash of the file
                },
                "...",
            } ]
        },
        "...",
    } ]
}
```

## Working with a local database
Here a database is a directory that contains files in which could be stored under a specific structure (tree or flat) but it is not necessary to be a database in the traditional sense.

### Without a registry file
The files can be loaded and saved by by some criteria, like:
- some _field_ of the filename
- some _extension_ of the filename
- combination of _fields_ and _extensions_

### With a registry file
The file can be loaded and saved by using the registry file.

## Working without a local database
At first could happen that files can be simply tiny `FILE` objects fetched by some metadata from the internet (url) or created by the user (emulates a regular file).

When these `FILE`s has assigned to some existing file in the OS file system host, they can be saved into a:
* local database                (content is copied)
* local file system directory   (content is moved)
* registry file                 (only filepath and md5 hash of the file)

## Database manager
Any database has a directory where the data is stored.
This directory is model by `db_dir.py` file and can handle the following operations:
- Create a new database directory
- Describe the database director structure
- Describe the database director root path
- Describe a database directory node
- Describe the status of the database directory
- Find any file in the database directory
- Describe a summary of the database directory
- Describe a summary of an entry in the database directory

Also, it has a database loader that can be used to load the database. The database loader is defined by the `db_loader.py` file and can handle the following operations:
- Load a database directory
- Set the database directory to be loaded
- Describe the database directory that is being loaded
- Describe all the files in the database directory
- Describe a summary of the database directory
- Describe a summary of an entry in the database directory
- Work with a database registry file
- Describes which is the registry file that is being used (loaded)
- List all the records dates of the registry file
- Load all files from a specific record of the registry file
- Verify if a file is in the registry file
- Save a record in the registry file

Finally, exists an abstraction of a registry. A registry is a file that stores the information of files using `records`. The structure of the registry is defined in `db_registry.py` file and can handle the following operations:
- Load a registry file
- Create a new registry file
- Describe the registry file location
- Describe the registry file creation date
- Describe the registry file modification date
- Load a record from the registry file
- Create a new record in the registry file
- Lookup a record in the registry file
- List all records dates of the registry file

## File manager
Any file has a filename and metadata that can be used to perform some operations. To operate with files, defines the following operations:
- Create a new file
- Load a file
- Describe the file properties
- Compress or decompress a file
- Set some properties to the file, like location, HTTP headers and URL
- Describe a md5 hash of the file
- Comparate two files
- Order a list of files
- Download a file from the internet
- Remembers downloaded files
- Save a file in a database directory


## Installation
To install the package, run the following command:

```bash
pip install datamanager
```

## Usage
The following code shows how to use the data manager to load a database directory and load a file:

---
**Database manager**

```python
from datamanager import DatabaseManager
```

```python
dm = DatabaseManager()
```

```python
# Describe the database directory structure
print(dm)
```

```python
# Set the database directory root path
dm.set_db_directory()
```

```python
# Describe the database directory root path
dm.get_db_directory()**Others**
```

```python
# Load all files from a database directory
dm.load_db_dir('formats')
```


```python
# Describe a list of files in the database directory
dm.get_local_files()
```

```python
# Summary of the database directory

dm.get_summary('db_dir')
```

```python
# Summary of an entry in the database directory

dm.get_summary_of('id')
```

```python
# Load a database directory

dm.set_registry('filepath')
```

```python
# Describes the filepath of the registry file

dm.get_registry_file('filepath')
```

```python
# Load a record from the registry file

dm.load_from_registry('date')
```

```python
# Save loaded record in the registry file

dm.save_record()
```

```python
# Describes all records dates of the registry file

dm.dates()
```

```python
# Describes the registry file that is being used (loaded)

dm.print_registry()
```

```python
# Verify each given file in the registry file
dm.verified_files('files')
```

----
**Filemanager**
```python
from datamanager import FileManager
```

```python
# Create a new file manager
fm = FileManager()
```

```python
# Create a new file
file = fm.create('file.txt')
```

```python
# Describe the file properties
print(file)
```

```python
# Set some properties to the file
file.set_location('filepath')
file.set_httpheaders({'Content-Type': 'text/plain'})
file.set_url('http://example.com/file.txt')
```

```python
# Describe a md5 hash of the file
file.get_md5_signature()
```

---
**Registry**

```python
from datamanager import Registry
```

```python
registry = Registry()
```

```python
# Create a new registry file
registry.create('registry')
```

```python
# Get location of the registry file
registry.location()
```

```python
# Describes creation date of the registry file
registry.creation_time()
```

```python
# Describes modification date of the registry file
registry.modification_time()
```

```python
# Load a registry file
registry.load('filepath')
```

```python
# Describes the list of files of the given record
registry.files('record')
```

```python
# Load record from the registry file
registry.load_record('date')
```

```python
# Describes the record of the given date
registry.lookup_record('date')
```

```python
# Save record in the registry file
registry.save_record('record')
```

```python
# Describes all records dates of the registry file
registry.dates()
```

```python
# Create a new record in the registry file
registry.create_new_record(['file'])
```

```python
# Verify a list of files in the registry file
registry.verify_files(['file'])
```

---
**Others**

```python
from datamanager.utils import SAVER, DOWNLOADER, PARSER, COMPRESSOR, COMPARATOR
```

```python
downloader = DOWNLOADER()
compressor = COMPRESSOR()
saver      = SAVER()
parser     = PARSER()
comparator = COMPARATOR()
```

```python
# Download a file from the internet
downloader.retrieve_version('file')
# Get the downloaded files
downloader.get_downloaded_files()
# Get last downloaded file
downloader.get_file()
```

```python
# Compress a file
compressor.compress(file)
# Decompress a file
compressor.decompress(file)
```

```python
# Save a file in a database directory
saver.save(file)
# Set parser to handle the file
parser.set_parser('file_parser')
```

```python
# Parse a file
parser.parse(file)
# Describes if the file is parsable
parser.is_parsable(file)
```

```python
# Set file1
comparator.set_filename1(file1)
# Set file2
comparator.set_filename2(file2)
# Describes which file is max
comparator.max()
# Describes which file is min
comparator.min()
# Describes if the files are equal
comparator.equal()
# Compare two files (file1 and file2)
comparator.compare(file1, file2)
# Order a list of files
comparator.order([file1, file2, file3])
```