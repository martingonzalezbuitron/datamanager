from abc import ABC, abstractmethod


class ORGANIZATION_FORMAT(ABC):
    def __init__(self, name, formats: list['ABSTRACT_FORMAT']) -> None:
        self.name       = name
        self.__fmts     = formats
        # self.__default  = None if not formats else formats[0]
        # self.__selected = None

    def __add__(self, other: 'ABSTRACT_FORMAT') -> list['ABSTRACT_FORMAT']:
        return self.__fmts + other if other not in self.__fmts else self.__fmts


    def __eq__(self, __value: list['ABSTRACT_FORMAT']) -> bool:
        return f"{self.__hash__()}" == f"{hash(__value)}"
        # return self.__fmts == __value

    @abstractmethod
    def supported(self) -> list['ABSTRACT_FORMAT']:
        """
        List all supported formats.
        """
        return self.__fmts

    @abstractmethod
    def default(self) -> 'ABSTRACT_FORMAT':
        """
        Default format to be used in the fetch process.

        Returns: 'ABSTRACT_FORMAT'
        """
        pass

    @abstractmethod
    def select(self, formats: list['ABSTRACT_FORMAT']) -> list['ABSTRACT_FORMAT']:
        """
        Select formats from available formats to be used in the fetch process.

        Parameters:
        * formats: list['ABSTRACT_FORMAT']. List of formats to be selected.

        Returns: list['ABSTRACT_FORMAT']. List of selected formats.
        """
        formats = self.__valid_fmts(formats)
        return formats

    @abstractmethod
    def accepted(self) -> list['ABSTRACT_FORMAT']:
        """
        Accepted formats to be used in the fetch process. 

        Returns: list['ABSTRACT_FORMAT']. List of accepted formats.
        """
        pass

    @abstractmethod
    def documentation(self) -> str:
        """
        URL to the documentation of file formats.
        """
        pass

    def __valid_fmts(self, formats: list['ABSTRACT_FORMAT']) -> list['ABSTRACT_FORMAT']:
        """
        Describe which given formats are valid and which are not valid.

        Parameters:
        * formats: list['ABSTRACT_FORMAT']. List of formats to be selected.

        Returns: list['ABSTRACT_FORMAT']. List of valid formats.
        """
        for fmt in formats:
            if not fmt in self.supported():
                formats.remove(fmt)
                print(f"[WARNING] : Given format '{fmt}' is not available in available formats")
        return formats


class ABSTRACT_FORMAT(ABC):
    def __init__(self) -> None:
        self.value      : str

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}"

    def __eq__(self, __value: 'ABSTRACT_FORMAT') -> bool:
        return self.value == __value.value

    @abstractmethod
    def extension(self) -> str:
        """
        Extension of the format.

        Returns: str
        """
        pass

    @abstractmethod
    def extensions(self) -> list[str]:
        """
        Extensions of the format.

        Returns: list[str]
        """
        pass

    @abstractmethod
    def documentation(self) -> str:
        """
        URL to the documentation of file formats.
        """
        pass