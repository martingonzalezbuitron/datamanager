from abc import ABC, abstractmethod
from enum import Enum
import gzip
import hashlib
import os
import shutil
from typing import Optional
from urllib.parse import urlparse


class BASE_EXTENSION_FMT():
    """
    It assumes that the filename has the following structure:
    <ext-1><separator><ext-2><separator>...<ext-n>
    """
    def __init__(self, separator: str) -> None:
        self.extension_separator = separator

    def get_extensions(self, filename) -> list[str]:
        return filename.split(self.extension_separator)

class BASE_FIELD_FMT():
    """
    It assumes that the filename has the following structure:
    <field-1><separator><field-2><separator>...<field-n>
    """
    def __init__(self, separator: str) -> None:
        self.field_separator = separator

    def get_fields(self, filename) -> list[str]:
        return filename.split(self.field_separator)

class BASE_VERSION_FMT():
    """
    This class represents a format filename version.
    It assumes that the filename has the following structure:
    <version_prefix><major><version_level_separator><minor>
    """
    def __init__( self
                , field           : str
                , prefix          : str
                , level_separator : str ) -> None:
        self.field           = field
        self.prefix          = prefix
        self.level_separator = level_separator
        self.major           = self.__get_major()
        self.minor           = self.__get_minor()
        self.__validate_version()


    def version(self) -> str:
        return f"{self.prefix}{self.major}{self.level_separator}{self.minor}"

    # Private methods
    def __validate_version(self) -> None:
        try:
            self.version() == self.field
        except ValueError:
            raise ValueError(f"[ERROR]  : Invalid version in '{self.version()}', it must be '{self.field}'")
        return

    def __get_major(self) -> int:
        value = self.field                      \
                .split(self.prefix)[1]          \
                .split(self.level_separator)[0]
        self.__validate_level_value(value=value)
        return int(value)

    def __get_minor(self) -> int:
        value = self.field                      \
                .split(self.prefix)[1]          \
                .split(self.level_separator)[1]
        self.__validate_level_value(value=value)
        return int(value)

    def __validate_level_value(self, value) -> None:
        try:
            value.isdigit()
        except ValueError:
            raise ValueError(f"[ERROR]  : Invalid type for level version in '{self.field}', it must be an integer and is '{value}'")
        return value.isdigit()

class BASE_GZIP_FMT():
    """
    It assumes that the filename has the following structure:
    <name><separator><compression>
    """
    def __init__(self, separator: str) -> None:
        self.compression_separator = separator

    def get_compression(self, filename) -> str:
        return self.__get_compression(filename) if self.has_gzip_extension(filename=filename) else ''

    def has_gzip_extension(self, filename) -> bool:
        return  filename.endswith(f'{self.compression_separator}gz')   or  \
                filename.endswith(f'{self.compression_separator}gzip')

    # Private methods
    def __get_compression(self, filename) -> str:
        return filename.split(self.compression_separator)[-1]

class BASE_VALIDATOR(ABC):
    """
    A base validator.
    """
    @abstractmethod
    def validate(self, value) -> str:
        pass

    @abstractmethod
    def is_valid(self, value) -> bool:
        pass

class VALIDATOR_URL(BASE_VALIDATOR):
    """
    """
    @staticmethod
    def validate(url: str) -> str:
        """
        Describes a valid URL.
        """
        assert isinstance(url, str), f"Given 'url' must be a string, not {type(url)}"
        try:
            VALIDATOR_URL.is_valid(url)
        except ValueError:
            raise ValueError(f"[ERROR]  : Invalid URL: {url}. Please, check the URL format. It must be 'scheme://netloc/path;parameters?query#fragment'")
        finally:
            return url

    @staticmethod
    def is_valid(url: str) -> bool:
        """
        Describes if the URL is valid.
        """
        assert isinstance(url, str), f"Given 'url' must be a string, not {type(url)}"
        parsed = urlparse(url)
        return all([parsed.scheme, parsed.netloc])

class VALIDATOR_LOCAL_FILE(BASE_VALIDATOR):
    """
    """
    @staticmethod
    def validate(filepath: str) -> str:
        """
        Describes a valid local file.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        if not VALIDATOR_LOCAL_FILE.is_valid(filepath):
            raise ValueError(f"[ERROR]  : Invalid filepath: {filepath}. Please, check the filepath format.")
        return filepath

    @staticmethod
    def is_valid(filepath: str) -> bool:
        """
        Describes if the filepath is valid.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        url_parsed = urlparse(filepath)
        return os.path.exists(url_parsed.path) if url_parsed.scheme in ('file', '') else False

class BASE_FILE_FMT(ABC):
    """
    It assumes that the filename has the following structure:
    <prefix_id><name_id><suffix_id>
    """
    def __init__( self
                , filename  : str
                , prefix_id : Optional[str] = None
                , suffix_id : Optional[str] = None ) -> None:
        assert isinstance(filename, str), f"Given 'filename' must be a string, not {type(filename)}"
        self.location         = filename if os.path.isfile(filename) else None
        self.raw              = os.path.basename(filename)
        self.prefix_id        = prefix_id
        self.suffix_id        = suffix_id
        self.name_id          = self.__get_name_id()

    @abstractmethod
    def __str__(self) -> str:
        return f"{self.prefix_id}{self.name_id}{self.suffix_id}"

    @abstractmethod
    def set_location(self, location: str) -> None:
        """
        Set the location of the file. Validate the location and move the file to the new location.
        """
        location = VALIDATOR_LOCAL_FILE.validate(filepath=location)
        if self.location is not None:
            try:
                os.makedirs(os.path.dirname(location), exist_ok=True)
                shutil.move(self.location, location)
            except Exception as e:
                raise e
        self.location = location
        return

    # Private methods
    def __get_name_id(self) -> str:
        if self.prefix_id is None and self.suffix_id is None:
            return self.raw
        elif self.suffix_id is None and self.prefix_id is not None:
            return self.raw.split(self.prefix_id)[1]
        elif self.suffix_id is not None and self.prefix_id is not None:
            return self.raw.split(self.prefix_id)[1].split(self.suffix_id)[0]
        else:
            return self.raw.split(self.suffix_id)[0]

class FILE_FIELDS(Enum):
    """
    Enum class to represent the fields of a FILE object.
    """
    prefix_id        : Optional[str] = None
    suffix_id        : Optional[str] = None
    field_separator  : str           = '_'
    extension_separator : str        = '.'

class FILE(BASE_FILE_FMT):
    """
    It assumes that the filename has the following structure:
    <prefix_id><name_id><suffix_id><fields><exts><compression>
    """
    def __init__( self
                , filename: str
                , prefix_id: Optional[str] = None
                , suffix_id: Optional[str] = None
                , field_separator: str = '_'
                , extension_separator: str = '.') -> None:
        super().__init__( filename  = filename
                        , prefix_id = prefix_id
                        , suffix_id = suffix_id )
        self.__base_field     = BASE_FIELD_FMT(separator=field_separator)
        self.__base_extension = BASE_EXTENSION_FMT(separator=extension_separator)
        self.__base_gzip      = BASE_GZIP_FMT(separator=extension_separator)
        self.name_id          = self.name_id.split(self.__base_field.field_separator)[0]
        self.fields           = self.__base_field.get_fields(self.raw.split(self.prefix_id)[1])
        self.fields           = self.fields[:-1] + [self.fields[-1].split(self.__base_extension.extension_separator)[0]]
        self.exts             = self.__base_extension.get_extensions(self.raw.split(self.__base_field.field_separator)[-1])[1:]
        self.url              = filename if VALIDATOR_URL.is_valid(filename) else None
        self.httpheaders      = None
        if self.__base_gzip.has_gzip_extension(self.raw):
            self.compression = self.__base_gzip.get_compression(self.raw)
            self.compressed  = True
            self.exts        = self.exts[:-1]
        else:
            self.compression = ''
            self.compressed  = False
        self.md5_signature   = self.__generate_md5_signature(self.location) if self.location is not None else None

    def __str__(self) -> str:
        return super().__str__()                       + \
                f"{self.__base_field.field_separator}" + \
                f"{self.fields}"                       + \
                f"{self.__base_field.field_separator}" + \
                f"{self.__base_extension.extension_separator.join(self.exts)}" + \
                f"{self.compression}"

    def __eq__(self, other: 'FILE') -> bool:
        assert isinstance(other, FILE), f"Given 'other' must be a FILE object, not {type(other)}"
        return self.raw == other.raw

    def __ne__(self, other: 'FILE') -> bool:
        assert isinstance(other, FILE), f"Given 'other' must be a FILE object, not {type(other)}"
        return self.raw != other.raw

    def __hash__(self) -> int:
        return hash(self.raw)

    # Public methods
    def set_location(self, location: str) -> None:
        """
        Set the location of the file. Validate the location and move the file to the new location. Also checks and set the MD5 signature.
        """
        value = self.__generate_md5_signature(location)
        if self.md5_signature is not None and self.md5_signature != value:
            raise ValueError(f"[ERROR]  : Invalid MD5 signature for '{location}'")
        elif self.md5_signature is None:
            self.md5_signature = value
        super().set_location(location)
        return

    def set_httpheaders(self, headers: dict) -> None:
        """
        Set the HTTP headers of the file in the server.
        """
        assert isinstance(headers, dict), f"Given 'headers' must be a dictionary, not {type(headers)}"
        self.httpheaders = headers
        return

    def set_url(self, url: str) -> None:
        self.url = VALIDATOR_URL.validate(url=url)
        return

    def get_md5_signature(self) -> Optional[str]:
        """
        Get the MD5 signature of the file.
        """
        return self.md5_signature

    def __generate_md5_signature(self, filepath :str) -> str:
        """
        Generate the MD5 signature of the file.

        Comment: To ensure always get the same hash for a file, always open the file
        in binary mode. Otherwise, the hash may change depending on the platform.
        """
        if self.compressed:
            with gzip.open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        else:
            with open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        return value

