
import os
import shutil
from typing import TypeVar
from datamanager.filemanager.fileparser import PARSER
from datamanager.filemanager.compressor import FILE_GZIPPER
from datamanager.filemanager.filenames import FILE as BFILE

import logging
logger = logging.getLogger(__name__)

FILE = TypeVar('FILE', bound='BFILE')


class FILE_SAVER():
    """
    This class saves a file in a given output directory.
    """
    def __init__( self
                , file       : 'FILE'
                , output_dir : str  = os.getcwd()
                , overwrite  : bool = False
                , compress   : bool = True
                , verbose    : bool = False ):
        self.__file       = file
        self.__output     = output_dir if output_dir.endswith('/') else f"{output_dir}/"
        self.__overwrite  = overwrite
        self.__compress   = compress
        self.__verbose    = verbose
        self.__compress   = compress
        self.__parser     = PARSER()
        self.__compressor = FILE_GZIPPER(self.__file, self.__parser)
    
        if self.__compress:
            if not self.__file.compressed:
                self.__file = self.__compressor.compress() # FIXME: Who gives the parser?
        else:
            if self.__file.compressed:
                self.__file = self.__compressor.decompress()
        self.__filepath = f"{self.__output}{self.__file.raw}"
        self.__output_exists = os.path.exists(self.__output)
        self.__filepath_exists = os.path.exists(self.__filepath)

    def set_parser(self, file_parser: 'PARSER') -> None:
        """
        Set a PARSER object.

        Parameters:

        * file_parser: a PARSER object.

        Returns: nothing
        """
        self.__parser     = file_parser
        self.__compressor = FILE_GZIPPER(self.__file, self.__parser)
        return

    def save(self) -> None:
        """
        Save a FILE in a given output directory.
        """
        if not self.__filepath_exists:
            if self.__verbose:
                logger.info(f"[ INFO ]: Saving file '{self.__file.raw}' in '{self.__output}' ...")
            if not self.__output_exists:
                if self.__verbose:
                    logger.info(f"[ INFO ]: Creating directory '{self.__output}' ...")
                os.makedirs(self.__output)
            filepath = shutil.copy(self.__file.location, self.__output)
            os.rename(f"{filepath}", self.__filepath)
            self.__file.location = self.__filepath

        else:
            if self.__verbose:
                logger.info(f"[ INFO ]: File '{self.__filepath}' already exists.")
            if self.__overwrite:
                if self.__verbose:
                    logger.info(f"[ INFO ]: Overwriting file '{self.__filepath}' ...")
                os.remove(self.__filepath)
                logger.info(f"[ INFO ]: Saving file '{self.__file.raw}' in '{self.__output}' ...")
                filepath = shutil.copy(self.__file.location, self.__output)
                os.rename(f"{filepath}", self.__filepath)
                self.__file.location = self.__filepath
        return

