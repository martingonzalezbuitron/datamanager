from typing import Optional, TypeVar

from datamanager.filemanager.filenames import FILE as BFILE

import logging
logger = logging.getLogger(__name__)

from urllib.request import urlretrieve

FILE = TypeVar('FILE', bound='BFILE')


class FILE_DOWNLOADER():
    def __init__( self
                , server
                , verbose = False ):
        """
        Create a FILE_DOWNLOADER object.

        Parameters:

        * server : str. The server to be used to download files.
        * verbose : bool. If set to True, active verbosity. (Default: False)

        Returns: nothing
        """
        self.__server       : str            = server
        self.__verbose      : bool           = verbose                       # enable or disable verbose
        self.__file         : Optional['FILE'] = None
        self.__downloaded   : list['FILE'] = []


    def get_downloaded_files(self) -> list['FILE']:
        """
        """
        return self.__downloaded

    def retrieve_version(self, file_version: 'FILE') -> tuple['FILE', dict]:
        """
        Download a gzip file of a specific version of a FILE using http protocol.

        Parameters:

        * file_version : FILE. A FILE object to be downloaded.

        Returns: tuple. A tuple with the filepath and a HTTP message headers.
        """
        # try:
        #     self.__server == file_version.url
        try: #TODO - Future improvement: https://realpython.com/python-download-file-from-url/
            filepath, http_headers = urlretrieve(url=f"{file_version.url}")
        except Exception as e:
            raise logger.error(f"[ERROR]: {e}. Not possible to fetch '{file_version.url}'")
        finally:
            logger.debug(f"Retrieved '{file_version}' as '{filepath}'. Changing location and adding HTTP headers as another attribute...")
            file_version.set_location(filepath)
            file_version.set_httpheaders(headers=dict(http_headers))
            self.__file = file_version # Remember the last downloaded file
            self.__downloaded.append(file_version)
        return file_version, http_headers

    def get_file(self) -> 'FILE':
        """
        Get the FILE that was downloaded.
        """
        if self.__file is None:
            raise ValueError(f"[ERROR]: Please, download a file first.")
        return self.__file


