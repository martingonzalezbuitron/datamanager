import os
from typing import TypeVar
from urllib.parse import urlparse
from datamanager.filemanager.filenames import FILE as BFILE
from datamanager.filemanager.factories import FACTORY_FILE
from datamanager.filemanager.formats.abstract import ABSTRACT_FORMAT

FILE = TypeVar('FILE', bound='BFILE')


class PARSER(object):
    """
    Parser class to parse a file.
    """
    def __init__(self, factory_file: 'FACTORY_FILE' = FACTORY_FILE()) -> None:
        self.__factory = factory_file

    def __repr__(self) -> str:
        return f"{PARSER.__name__}"

    def __str__(self) -> str:
        return  f"{PARSER.__name__}(using '{self.__factory}')"

    def parse(self, filepath: str) -> 'FILE':
        """
        Parse a filepath of a file and return an instance of FILE or any of its subclasses.
        The given filepath could be a local file or a URL to a file.

        Parameters:

        * filepath : str. A filepath of file or a URL of a file.

        Returns: a FILE instance or any of its subclasses.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"

        return self.__factory.create(filepath=filepath)

    def is_parsable(self, filepath: str, formats: list['ABSTRACT_FORMAT']) -> bool:
        """
        Describes if the filepath is parsable.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        return self.__factory.is_parsable(filepath=filepath, formats=formats)
    # Private methods

    def __is_local(self, url):
        """
        Describes if the URL is a local file.
        Taken from: https://stackoverflow.com/questions/68626097/pythonic-way-to-identify-a-local-file-or-a-url
        """
        url_parsed = urlparse(url)
        return os.path.exists(url_parsed.path) if url_parsed.scheme in ('file', '') else False

