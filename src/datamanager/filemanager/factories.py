from abc import ABC, abstractmethod
import os
from typing import Type, TypeVar

from datamanager.filemanager.filenames import FILE as BFILE, FILE_FIELDS
from datamanager.filemanager.formats.abstract import ABSTRACT_FORMAT

FILE = TypeVar('FILE', bound='BFILE')


class FACTORY_FILE_BASE(ABC):
    """
    Factory class to create instances of subclasses of FILE.
    """

    @abstractmethod
    def create(self, filepath: str) -> 'FILE':
        pass

    @abstractmethod
    def is_parsable(self, filepath: str,  formats: list['ABSTRACT_FORMAT']) -> bool:
        """
        Describes if the filepath is parsable.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        pass

    @abstractmethod
    def build_filename(self, name_id: str) -> str:
        """
        Build a filename based on the given name_id.
        """
        pass


class FACTORY_FILE(FACTORY_FILE_BASE):
    """
    Factory class to create an instance of FILE or subclasses of it.
    """
    def __init__(self, class_type: Type[FILE] = BFILE) -> None:
        self.class_type = class_type

    def __str__(self) -> str:
        return f"{FACTORY_FILE.__name__}"

    def create(self, filepath: str) -> 'FILE':
        return self.class_type(filename=filepath)

    def is_parsable(self, filepath: str, formats: list[ABSTRACT_FORMAT]) -> bool:
        """
        Describes if the filepath is parsable.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        filename = os.path.basename(filepath)
        return  (FILE_FIELDS.prefix_id.value is None or filename.startswith(FILE_FIELDS.prefix_id.value)) and \
                (FILE_FIELDS.suffix_id.value is None or filename.endswith(FILE_FIELDS.suffix_id.value))   and \
                FILE_FIELDS.extension_separator.value in filename and \
                FILE_FIELDS.field_separator.value in filename and \
                any([fmt.extension() in filename for fmt in formats])

    def build_filename(self, name_id: str, fields: list = None, exts: list = None, compression: str = None) -> str:
        """
        Build a filename based on the given name_id.
        It assumes that the filename has the following structure:
            <prefix_id><name_id><suffix_id><fields><exts><compression>
        """
        assert isinstance(name_id, str), f"Given 'name_id' must be a string, not {type(name_id)}"
        assert fields is None or isinstance(fields, list), f"Given 'fields' must be a list, not {type(fields)}"
        assert exts is None or isinstance(exts, list), f"Given 'exts' must be a list, not {type(exts)}"
        assert compression is None or isinstance(compression, str), f"Given 'compression' must be a string, not {type(compression)}"

        filename = ''
        if FILE_FIELDS.prefix_id.value is not None:
            filename += FILE_FIELDS.prefix_id.value
        filename += name_id
        if FILE_FIELDS.suffix_id.value is not None:
            filename += FILE_FIELDS.suffix_id.value
        if fields is not None:
            for field in fields:
                filename += FILE_FIELDS.field_separator.value + field
        if exts is not None:
            for ext in exts:
                filename += FILE_FIELDS.extension_separator.value + ext
        if compression is not None:
            filename += FILE_FIELDS.extension_separator.value + compression
        return filename