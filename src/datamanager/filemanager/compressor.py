import os
import gzip
import shutil
from typing import TypeVar
from datamanager.filemanager.filenames import FILE as BFILE
from datamanager.filemanager.fileparser import PARSER

import logging
logger = logging.getLogger(__name__)

FILE = TypeVar('FILE', bound='BFILE')


class FILE_GZIPPER():
    """
    This class compresses and decompresses a FILE object.
    It uses the gzip library to compress and decompress files.
    After operate a file, it removes the original file and
    returns a new FILE object that is the result of
    the operation.
    """
    def __init__(self, file: 'FILE', parser: PARSER):
        """
        Initialize the class with a FILE object and a PARSER object.

        Parameters:

        * file : FILE. A FILE object to be compressed or decompressed.

        * parser : PARSER. A PARSER object to be used to parse the FILE object.

        Returns: nothing
        """
        self.__file   = file
        self.__parser = parser

    def compress(self) -> 'FILE':
        """
        Compress a FILE using gzip and return a new FILE object.
        """
        if self.__file.compression:
            return
        try:
            with open(self.__file.raw, 'rb') as f_in, gzip.open(f"{self.__file.raw}.gz", 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            os.remove(self.__file.raw)
        except Exception as e:
            raise e(f"[ERROR]: {e}")
        return self.__parser.parse(f"{self.__file.raw}.gz")

    def decompress(self) -> 'FILE':
        """
        Decompress a FILE using gzip and return a new FILE object.
        """
        if not self.__file.compression:
            return
        try:
            with gzip.open(self.__file.raw, 'rb') as f_in, open(self.__file.raw.split('.gz')[0], 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
            os.remove(self.__file.raw)
        except Exception as e:
            raise e(f"[ERROR]: {e}")
        return self.__parser.parse(self.__file.raw.split('.gz')[0])

