from typing import Optional, TypeVar

from datamanager.filemanager.filenames import FILE as BFILE

FILE = TypeVar('FILE', bound='BFILE')

class COMPARATOR(object):
    """This class compares the versions of the files
    taking into account major and minor versions. In which the
    major version is the first number (int) and the minor is the
    second number (int) separated by a minus sign (-) and having a
    prefix (v) like this:
    <filename><maybe_separator><major>-<minor>.<ext>

    <wwPDB_ID>                                                  # <prefix_id><4-characters>, e.g. <pdb_0000><3jce>
    <field_separator>                                           # e.g. _
        <content_type>                                          # e.g. xyz
    <field_separator>                                           # e.g. _
        <prefix_version>                                        # e.g. v
            <major_version><version_separator><minor_version>   # e.g. 1-4
        <ext_separator>                                         # e.g. .
            <file_format_type>                                  # e.g. cif
        <ext_separator>                                         # e.g. .
            <file_compression_type>                             # e.g. gz
    """
    def __init__(self):
        self.__filename1  : Optional['FILE'] = None
        self.__filename2  : Optional['FILE'] = None
        self.__version_f1 : Optional[int]    = None
        self.__version_f2 : Optional[int]    = None

    def __str__(self):
        return f"Filename 1: {self.__filename1}\nFilename 2: {self.__filename2}"

    def __repr__(self):
        return f"COMPARATOR({self.__str__()})"

    def equals(self) -> bool:
        return self.__version_f1 == self.__version_f2

    def max(self) -> 'FILE':
        return self.__filename1 if self.__version_f1 > self.__version_f2 else self.__filename2

    def min(self) -> 'FILE':
        return self.__filename1 if self.__version_f1 < self.__version_f2 else self.__filename2

    def set_filename1(self, filename: 'FILE') -> None:
        """
        """
        assert isinstance(filename, FILE), f"filename must be a FILE object, not {type(filename)}"
        if self.__filename2 is not None:
            self.__validate(filename, self.__filename2)
        self.__filename1 = filename
        self.__version_f1 = self.__get_version(filename)

    def set_filename2(self, filename: 'FILE') -> None:
        """
        """
        assert isinstance(filename, FILE), f"filename must be a FILE object, not {type(filename)}"
        if self.__filename1 is not None:
            self.__validate(filename, self.__filename1)
        self.__filename2 = filename
        self.__version_f2 = self.__get_version(filename)

    def compare(self, filename1: 'FILE', filename2: 'FILE') -> bool:
        """
        Describes if the versions of the files are equal.
        """
        self.set_filename1(filename1)
        self.set_filename2(filename2)
        return self.equals()

    @staticmethod
    def order(files: list['FILE']) -> list['FILE']:
        """
        Given a list of FILE, return them in a decreasing order.

        Parameters:

        files : list of FILE

        Returns: list of FILE
        """
        assert isinstance(files, list), f"Given 'files' must be a list, not {type(files)}"
        assert all([isinstance(file, BFILE) for file in files]), f"Given 'files' must be a list of FILE, not {files}"
        return sorted(files, key=lambda file: (file.major, file.minor), reverse=True)

    def __get_version(self, filename: 'FILE') -> int:
        return int(str(filename.major)+str(filename.minor))

    def __validate(self, f1: 'FILE', f2: 'FILE') -> None:
        """
        """
        assert f1 != f2, f"Both files must be different, not '{f1}' and '{f2}'"
        assert f1.name_id == f2.name_id, f"Both names must be the same, not '{f1.name_id}' and '{f2.name_id}'"
        assert f1.exts == f2.exts, f"Both extensions must be the same, not '{f1.exts}' and '{f2.exts}'"

