from dataclasses import dataclass
from datetime import datetime
import hashlib
import os
import gzip
import json
import shutil
import tempfile
from typing import Optional, TypeVar
from urllib.parse import urlparse
from urllib.request import urlretrieve

from datamanager.filemanager.fileparser import PARSER
from datamanager.filemanager.filenames import FILE as BFILE

import logging
logger = logging.getLogger(__name__)

FILE = TypeVar('FILE', bound='BFILE')


class DB_REGISTRY():
    """
    Database registry. All database records must inherit from this class.

    The registry file must be:
        - a JSON file
        - gzip compressed

    Every file in the registry must have:
        - a MD5 signature
        - a filepath
        - a URL or None (if the file was remotely downloaded, otherwise, is considered created locally)

    The modification time of the registry file must be same as the creation time of the last record.

    Registry format:
    {
        "location"          : str, # Absolute path to the registry file
        "description"       : str, # Description of the registry file
        "database_dir"      : str, # Absolute path to the database directory
        "creation_time"     : str, # YYYY-MM-DD HH:MM:SS, creation time of the registry file
        "modification_time" : str, # YYYY-MM-DD HH:MM:SS, last modification time of the registry file
        "records"           : [ {
                "description"       : str, # Description of the record
                "creation_time"     : str, # YYYY-MM-DD HH:MM:SS, creation time of the record
                "nb_files"          : int, # Number of files in the record
                "files"             : [ {
                    "url"               : str,  # URL to the file
                    "filepath"          : str,  # Absolute path to the file
                    "md5_signature"     : str,  # MD5 hash of the file
                    },
                    "...",
                } ]
            },
            "...",
        } ]
    }
    """
    def __init__(self, db_root: str, parser: Optional['PARSER'] = None, verbose: bool = True) -> None:
        """
        Initialize the database registry.

        Parameters:

            * db_dir : DB_DIR. The database directory.
            * parser : PARSER. The file parser. (Default: None, it will be
                    created by the class using 'FACTORY_FILE' as format file)
            * verbose : bool. If set to True, active verbosity. (Default: True)

        Returns: nothing
        """
        self.__db_dir   = db_root if db_root.endswith("/") else f"{db_root}/"
        self.__parser   = PARSER() if parser is None else parser
        self.__verbose  = verbose
        self.__filepath = None
        self.__tmpfile  = None
        self.__hash     = None

    def __repr__(self) -> str:
        return  f"{DB_REGISTRY.__name__}\n" + \
                f"  db_dir  : {self.__db_dir}\n" + \
                f"  parser  : {self.__parser}\n" + \
                f"  verbose : {self.__verbose}"

    def __str__(self) -> str:
        return  f"{DB_REGISTRY.__name__}\n"      + \
                f"  filepath: {self.__filepath}\n" + \
                f"  db_dir  : {self.__db_dir}\n" + \
                f"  parser  : {self.__parser}\n" + \
                f"  verbose : {self.__verbose}"

    def create(self, registry_file: str, description: str) -> None:
        """
        Create a new JSON registry file and save it gzip compressed.
        """
        assert isinstance(registry_file, str), f"Given 'registry_file' must be a string, not {type(registry_file)}"
        assert not os.path.exists(registry_file), f"Given 'registry_file' must not exist"
        assert isinstance(description, str), f"Given 'description' must be a string, not {type(description)}"
        self.__filepath = f"{registry_file}.json.gz" if not registry_file.endswith(".json.gz") else registry_file
        self.__msg(f"Creating a new registry file ...")
        # Use REGISTRY_FILE dataclass
        file = REGISTRY_FILE( location          = self.__filepath
                            , description       = description
                            , database_dir      = self.__db_dir
                            , creation_time     = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            , modification_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            , records           = [] )
        self.__save(file)
        self.__work_with_tempfile()
        return

    def location(self) -> str:
        """
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
            if self.__filepath != registry.get("location"):
                raise ValueError(f"[ERROR]  : The registry file '{self.__filepath}' is not the same as the one defined in the registry.")
        return self.__filepath

    def description(self) -> str:
        """
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        return registry.get("description")

    def creation_time(self) -> str:
        """
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        return registry.get("creation_time")

    def modification_time(self) -> str:
        """
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        return registry.get("modification_time")

    def load(self, registry_file: str) -> None:
        """
        Load the given registry file.
        """
        self.__validate_registry_file(registry_file)
        self.__msg(f"Loading the registry file '{registry_file}' ...")
        self.__filepath = registry_file
        self.__work_with_tempfile()
        return

    def files(self, record: 'RECORD') -> list['FILE']:
        """
        Get the files as a list of FILE from the registry record.
        If some MD5 signature of a file is different from the one in the registry, raise an error.

        Parameters:

            * record : dict, a registry record.

        Returns: list[FILE]
        """
        self.__check_registry()
        files = []
        corrupted_files = tuple()
        for file_dict in record.get("files"):
            file = self.__parser.parse(file_dict['location']) if file_dict['location'] is not None else self.__parser.parse(file_dict['url'])
            file.set_location(file_dict['filepath'])
            if file.md5_signature != file_dict['md5_signature']:
                corrupted_files += (file, file.md5_signature, file_dict['md5_signature'])
            files.append(file)
        if len(corrupted_files) > 0:
            msg = '\n'.join([file.location+'\t'+md5+'\t'+reg_md5 for file, md5, reg_md5 in corrupted_files])
            raise ValueError(f"[ERROR]  : The following files are corrupted:\n{msg}")
        return files

    def load_record(self, date: Optional[str]) -> 'RECORD':
        """
        Load the registry record with the given date.
        """
        assert date is None or isinstance(date, str), f"Given 'date' must be a string, not {type(date)}"
        self.__check_registry()
        if self.__tmpfile is None:
            self.__work_with_tempfile()
        if date is None:
            raise ValueError(f"[ERROR]  : Please, provide a valid date to load the record to use in the registry.")
        record = self.lookup_record(date)
        return RECORD( description   = record.get("description")
                    ,  creation_time = record.get("creation_time")
                    ,  nb_files      = int(record.get("nb_files"))
                    ,  files         = self.__create_files_of_record(self.__load_files_from_record(record)))

    def lookup_record(self, date: str) -> dict:
        """
        Lookup the record with the given date.
        """
        self.__check_registry()
        if self.__tmpfile is None:
            self.__work_with_tempfile()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        for record in registry.get("records"):
            if record.get("creation_time") == date:
                break
        if len(registry.get("records")) == 0 or record.get("creation_time") != date:
            raise ValueError(f"[ERROR]  : The registry record with the date '{date}' does not exist.")
        return record

    def save_record(self, record: 'RECORD') -> None:
        """
        Save the record to the registry file.
        """
        self.__check_registry()
        self.__validate_record(record)
        self.__msg(f"Saving the record to registry file ...")
        self.__save_record(record)
        return

    def dates(self) -> list[str]:
        """
        Get the dates of all records in the registry file.
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        return [record.get("creation_time") for record in registry.get("records")]

    def last_date(self) -> str:
        """
        Get the date of the last record in the registry file.

        Returns: str (date in the format 'YYYY-MM-DD HH:MM:SS')
        """
        self.__check_registry()
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)

        if len(registry.get("records")) == 0:
            raise ValueError(f"[ERROR]  : There is no record in the given registry.")
        record = registry.get("records")[-1]
        last_date = record.get("creation_time")
        if last_date != registry.get("modification_time"):
            raise ValueError(f"[ERROR]  : The registry file '{registry.get('location')}' has been modified on {registry.get('modification_time')} and the last record is from {last_date}. This means that the registry file has been modified after the last record. Please, check the registry file because it may be corrupted.")
        logger.info(f"Using the latest record with creation date: '{last_date}'")
        return last_date

    def create_new_record(self, description: str, files: list['FILE']) -> 'RECORD':
        """
        Create a new record from a list of FILE objects.
        """
        assert isinstance(description, str), f"Given 'description' must be a string, not {type(description)}"
        self.__check_registry()
        files = self.__create_files_of_record(files)
        self.__validate_files_record(files)
        return RECORD( description   = description
                    ,  creation_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    ,  nb_files      = len(files)
                    ,  files         = files )

    def verified_files(self, files: list['FILE']) -> list['FILE']:
        """
        Get the verified files.
        """
        self.__check_registry()
        verified_files = []
        for file in files:
            if file.md5_signature == hashlib.md5(open(file.location, 'rb').read()).hexdigest():
                verified_files.append(file)
        return verified_files

    # Private methods
    def __check_registry(self) -> None:
        """
        Check if the registry file is loaded.
        """
        if self.__filepath is None:
            raise ValueError(f"[ERROR]  : No registry file is loaded. Please, load a registry file.")
        return

    def __create_files_of_record(self, files: list['FILE']) -> list['RECORD_FILE']:
        """
        Create a list of RECORD_FILE objects from a list of FILE objects.
        """
        return [RECORD_FILE(file=file) for file in files]

    def __validate_registry_file(self, registry_file: str) -> None:
        """
        Check if the registry file is a JSON file and is gzip compressed.
        """
        # Ask mimetype with os library
        if not os.path.isfile(registry_file):
            raise ValueError(f"[ERROR]  : The registry file '{registry_file}' does not exist.")
        elif not registry_file.endswith(".gz"):
            raise ValueError(f"[ERROR]  : The registry file '{registry_file}' must be a compressed file.")
        elif not registry_file.endswith(".json.gz"):
            raise ValueError(f"[ERROR]  : The registry file '{registry_file}' must be a compressed JSON file.")
        with gzip.open(registry_file, mode="rt", encoding='UTF-8') as f:
            try:
                json.load(f)
            except json.JSONDecodeError:
                raise ValueError(f"[ERROR]  : The registry file '{registry_file}' is not a JSON file.")
        return

    def __validate_record(self, record: 'RECORD') -> None:
        """
        Save the record to the registry file.
        """
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)
        if self.__has_record_existence(registry.get("records"), record):
            raise ValueError(f"[ERROR]  : The record with the date '{record.creation_time}' already exists in the registry.")
        elif record.nb_files() != len(record.files()):
            raise ValueError(f"[ERROR]  : Please, provide a valid record. Must have equal number of files and the number of files in the record.")
        self.__validate_files_record(record.files())
        return

    def __validate_record_date(self, record: 'RECORD', registry: dict) -> None:
        """
        Check if record date is greater than the last modification time
        """
        if record.creation_time() <= registry.get("modification_time"):
            raise ValueError(f"[ERROR]  : The record date '{record.creation_time()}' must be greater than the last modification time '{registry.get('modification_time')}'")
        return 

    def __validate_files_record(self, files: list['RECORD_FILE']) -> None:
        """
        Validate the files with RECORD_FILE structure to be used in the registry.
        """
        for idx, file in enumerate(files):
            if not (file.url() is None or isinstance(file.url(), str) and \
                    isinstance(file.filepath(), str)                      and \
                    isinstance(file.md5_signature(), str)):
                raise ValueError(f"[ERROR]  : Please, provide a valid file record for element with index '{idx}' . Must have 'url', 'filepath' and 'md5_signature' keys.")
        return

    def __load_records(self, records: list[dict]) -> list['RECORD']:
        """
        """
        return [ RECORD(  description   = record.get("description")
                        , creation_time = record.get("creation_time")
                        , nb_files      = int(record.get("nb_files"))
                        , files         = self.__create_files_of_record(self.__load_files_from_record(record)) ) for record in records ]

    def __save_record(self, record: 'RECORD') -> None:
        """
        Save the record to the registry file.
        """
        with gzip.open(self.__tmpfile, mode="rt", encoding='UTF-8') as f:
            registry = json.load(f)

        if not ("records" in registry.keys() and isinstance(registry.get("records"), list)):
            raise ValueError(f"[ERROR]  : The registry file '{self.__filepath}' is corrupted. It does not have any records.")

        if self.__has_record_existence(registry.get("records"), record):
            raise ValueError(f"[ERROR]  : The record with the date '{record.creation_time}' already exists in the registry.")
        del registry

        with gzip.open(self.__tmpfile, 'rt', encoding='UTF-8') as f:
            # load file into memory
            registry = json.load(f)
            db_dir = registry['database_dir']
            registry["records"].append( { "description": record.description()
                                        , "creation_time": record.creation_time()
                                        , "nb_files": record.nb_files()
                                        , "files": [{ "url"           : file.url()
                                                    , "md5_signature" : file.md5_signature()
                                                    , "filepath"      : file.filepath().split(db_dir)[1] if file.filepath().startswith(db_dir) else file.filepath() } for file in record]} )
            self.__validate_record_date(record, registry)
            registry["modification_time"] = record.creation_time()
        with gzip.open(self.__tmpfile, mode="wt", encoding='UTF-8') as f:
            json.dump(registry, f)
        self.__msg(f"Saving the record to registry file ...")
        self.__copy_to_registry()
        self.__msg(f"Record saved to registry file '{self.__filepath}'")
        return

    def __copy_to_registry(self) -> None:
        """
        Copy the temporary file to the registry file.
        """
        if self.__hash == MD5_HASH.get(self.__filepath):
            shutil.copy(self.__tmpfile, self.__filepath)
            self.__hash = MD5_HASH.get(self.__filepath)
        else:
            raise ValueError(f"[ERROR]  : The MD5 signature of the registry file '{self.__filepath}' is different from the one it is known '{self.__hash}'")
        return

    def __save(self, file: 'REGISTRY_FILE') -> None:
        """
        Save the registry file.
        """
        with gzip.open(f"{self.__filepath}", mode="wt", encoding='UTF-8') as f:
            json.dump({   "location"          : file.location
                        , "description"       : file.description
                        , "database_dir"      : file.database_dir
                        , "creation_time"     : file.creation_time
                        , "modification_time" : file.modification_time
                        , "records"           : file.records }, f)
        return

    def __work_with_tempfile(self) -> None:
        """
        """
        temp_dir = tempfile.gettempdir()
        self.__tmpfile = f"{temp_dir}/{os.path.basename(self.__filepath)}"
        self.__hash = MD5_HASH.get(self.__filepath)
        try:
            shutil.copy(self.__filepath, f"{self.__tmpfile}")
        except Exception as e:
            raise e(f"[ERROR]: {e}. Not possible to copy '{self.__filepath}' to '{self.__tmpfile}'")
        return

    def __load_files_from_record(self, record: dict) -> list['FILE']:
        """
        Load the files from the record.
        """
        files = []
        for record_file in record.get("files"):
            file = self.__parser.parse(record_file['filepath'])
            files.append(file)
        return files

    def __has_record_existence(self, records: list[dict], record: 'RECORD') -> bool:
        """
        Describes if the record exists in the registry.
        """
        return any([rec.get('creation_time') == record.creation_time() for rec in records])

    def __msg(self, msg: str) -> None:
        """
        """
        if self.__verbose:
            logger.info(msg)
        return

@dataclass
class REGISTRY_FILE():
    """
    """
    location          : str
    description       : str
    database_dir      : str
    creation_time     : str
    modification_time : str
    records           : list['RECORD']

    def __str__(self) -> str:
        return f"REGISTRY_FILE({self.location}, {self.description}, {self.database_dir}, {self.creation_time}, {self.modification_time}, {self.records})"

    def __repr__(self) -> str:
        return f"REGISTRY_FILE({self.location}, {self.description}, {self.database_dir}, {self.creation_time}, {self.modification_time}, {self.records})"

    def __eq__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.location == other.location

    def __ne__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.location != other.location

    def __gt__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.creation_time > other.creation_time

    def __lt__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.creation_time < other.creation_time

    def __ge__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.creation_time >= other.creation_time

    def __le__(self, other: 'REGISTRY_FILE') -> bool:
        assert isinstance(other, REGISTRY_FILE), f"Given 'other' must be a REGISTRY_FILE object, not {type(other)}"
        return self.creation_time <= other.creation_time

    def __hash__(self) -> int:
        return hash(self.location)

    def __contains__(self, record: 'RECORD') -> bool:
        return any([rec.get("creation_time") == record.get("creation_time") for rec in self.records])

    def __len__(self) -> int:
        return len(self.records)

    def __iter__(self):
        return iter(self.records)

    def __getitem__(self, idx: int) -> dict:
        return self.records[idx]

    def __setitem__(self, idx: int, record: 'RECORD') -> None:
        self.records[idx] = record

    def __delitem__(self, idx: int) -> None:
        del self.records[idx]

    def append(self, record: 'RECORD') -> None:
        self.records.append(record)
        return

    def remove(self, record: 'RECORD') -> None:
        self.records.remove(record)
        return

    def clear(self) -> None:
        self.records.clear()
        return

    def sort(self, key=None, reverse: bool = False) -> None:
        self.records.sort(key=key, reverse=reverse)
        return

    def reverse(self) -> None:
        self.records.reverse()
        return

    def copy(self) -> 'REGISTRY_FILE':
        return REGISTRY_FILE(self.location, self.database_dir, self.creation_time, self.modification_time, self.records.copy())

    def count(self, record: 'RECORD') -> int:
        return self.records.count(record)

    def index(self, record: 'RECORD') -> int:
        return self.records.index(record)

    def extend(self, records: list['RECORD']) -> None:
        self.records.extend(records)
        return

    def insert(self, idx: int, record: 'RECORD') -> None:
        self.records.insert(idx, record)
        return

    def pop(self, idx: int) -> dict:
        return self.records.pop(idx)

@dataclass
class RECORD():
    """
    Registry record.
    """
    def __init__(self, description: str, creation_time: str, nb_files: int, files: list['RECORD_FILE']) -> None:
        """
        """
        assert isinstance(description, str), f"Given 'description' must be a string, not {type(description)}"
        assert isinstance(creation_time, str), f"Given 'creation_time' must be a string, not {type(creation_time)}"
        assert len(creation_time) == 19, f"Given 'creation_time' must have the format 'YYYY-MM-DD HH:MM:SS'"
        assert isinstance(nb_files, int), f"Given 'nb_files' must be an integer, not {type(nb_files)}"
        assert isinstance(files, list), f"Given 'files' must be a list, not {type(files)}"
        assert all(isinstance(file, RECORD_FILE) for file in files), f"Given 'files' must be a list of RECORD_FILE, not {files}"
        self.__description   = description
        self.__creation_time = creation_time
        self.__nb_files      = nb_files
        self.__files         = files

    def __repr__(self) -> str:
        return f"{RECORD.__name__}"

    def __str__(self) -> str:
        return f"{RECORD.__name__}"

    def __eq__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time == other.__creation_time

    def __ne__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time != other.__creation_time

    def __gt__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time > other.__creation_time

    def __lt__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time < other.__creation_time

    def __ge__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time >= other.__creation_time

    def __le__(self, other: 'RECORD') -> bool:
        assert isinstance(other, RECORD), f"Given 'other' must be a RECORD object, not {type(other)}"
        return self.__creation_time <= other.__creation_time

    def __hash__(self) -> int:
        return hash(self.__creation_time)

    def __contains__(self, file: 'RECORD_FILE') -> bool:
        return any([f.get("md5_signature") == file.get("md5_signature") for f in self.__files])

    def __len__(self) -> int:
        return len(self.__files)

    def __iter__(self):
        return iter(self.__files)

    def __getitem__(self, idx: int) -> dict:
        return self.__files[idx]

    def __setitem__(self, idx: int, file: 'RECORD_FILE') -> None:
        self.__files[idx] = file

    def __delitem__(self, idx: int) -> None:
        del self.__files[idx]

    def append(self, file: 'RECORD_FILE') -> None:
        self.__files.append(file)
        return

    def remove(self, file: 'RECORD_FILE') -> None:
        self.__files.remove(file)
        return

    def clear(self) -> None:
        self.__files.clear()
        return

    def sort(self, key=None, reverse: bool = False) -> None:
        self.__files.sort(key=key, reverse=reverse)
        return

    def reverse(self) -> None:
        self.__files.reverse()
        return

    def copy(self) -> 'RECORD':
        return RECORD(self.__description, self.__creation_time, self.__nb_files, self.__files.copy())

    def count(self, file: 'RECORD_FILE') -> int:
        return self.__files.count(file)

    def index(self, file: 'RECORD_FILE') -> int:
        return self.__files.index(file)

    def extend(self, files: list['RECORD_FILE']) -> None:
        self.__files.extend(files)
        return

    def insert(self, idx: int, file: 'RECORD_FILE') -> None:
        self.__files.insert(idx, file)
        return

    def pop(self, idx: int) -> dict:
        return self.__files.pop(idx)

    def description(self) -> str:
        return self.__description

    def creation_time(self) -> str:
        return self.__creation_time

    def nb_files(self) -> int:
        return self.__nb_files

    def files(self) -> list['RECORD_FILE']:
        return self.__files

@dataclass
class RECORD_FILE():
    """
    Registry record file.
    """
    def __init__(self, file: 'FILE') -> None:
        """
        Describes a file in the registry record. The file must exists locally.

        Parameters:

            * filepath : str. The absolute path to the file.
            * url : str. The URL to the file. (Default: None)

        TODO: Check if the file exists remotely too.

        Returns: nothing
        """
        assert isinstance(file, BFILE), f"Given file {file} must be a FILE, not {type(file)}"
        self.__file          = file
        self.__filepath      = file.location
        self.__url           = file.url
        self.__md5_signature = self.__generate_md5_signature(self.__filepath) if self.__filepath is not None else None
        

    def __repr__(self) -> str:
        return f"{RECORD_FILE.__name__}"

    def __str__(self) -> str:
        return f"{RECORD_FILE.__name__}"

    def __eq__(self, other: 'RECORD_FILE') -> bool:
        assert isinstance(other, RECORD_FILE), f"Given 'other' must be a RECORD_FILE object, not {type(other)}"
        return self.__md5_signature == other.__md5_signature

    def __ne__(self, other: 'RECORD_FILE') -> bool:
        assert isinstance(other, RECORD_FILE), f"Given 'other' must be a RECORD_FILE object, not {type(other)}"
        return self.__md5_signature != other.__md5_signature

    def __hash__(self) -> int:
        # Return the md5 of the content of filepath
        return self.__md5_signature

    def url(self) -> str:
        return self.__url

    def filepath(self) -> str:
        return self.__filepath

    def md5_signature(self) -> str:
        return self.__md5_signature

    def set_url(self, url: str, secure: False) -> None:
        """
        Set the URL of the file.
        """
        if secure:
            filepath_tmp = self.__retrieve_file(url)
            self.__check_md5_signature(filepath_tmp)
        self.__url = url
        return

    def set_filepath(self, filepath: str) -> None:
        self.__check_md5_signature(filepath)
        self.__filepath = filepath
        return

    def __retrieve_file(self, url: str) -> None:
        """
        Download a file from given URL and describes the temporal filepath where the it is stored.
        """
        assert isinstance(url, str), f"Given 'url' must be a string, not {type(filepath)}"

        parsed = urlparse(url)
        if not all([parsed.scheme, parsed.netloc]):
            raise ValueError(f"[ERROR]  : Invalid URL: {url}")
        try:
            filepath, http_headers = urlretrieve(url=url)
        except Exception as e:
            raise e(f"[ERROR]: {e}. Not possible to fetch '{filepath}'. These are the HTTP headers:\n{http_headers}")
        return filepath

    def __check_md5_signature(self, filepath: str) -> None:
        """
        Check if the MD5 signature of a filepath is the same as the one it is known.
        """
        if self.__md5_signature != self.__generate_md5_signature(filepath):
            raise ValueError(f"[ERROR]  : The MD5 signature of the filepath '{filepath}' is different from the one it is known '{self.__md5_signature}'")
        return

    def __generate_md5_signature(self, filepath :str) -> str:
        """
        Generate the MD5 signature of the file.

        Comment: To ensure always get the same hash for a file, always open the file
        in binary mode. Otherwise, the hash may change depending on the platform.
        """
        if self.__file.compressed:
            with gzip.open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        else:
            with open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        return value


class MD5_HASH():
    """
    Factory class to generate
    """
    @staticmethod
    def get(filepath: str) -> str:
        """
        Generate the MD5 signature of the file.

        Comment: To ensure always get the same hash for a file, it always be opened in
        binary mode. Otherwise, the hash may change depending on the platform.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        assert os.path.isfile(filepath), f"Given 'filepath' must be a file, not a directory"

        if  filepath.endswith(f'.gz')   or  \
            filepath.endswith(f'.gzip'):
            with gzip.open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        else:
            with open(filepath, 'rb') as f:
                value = hashlib.md5(f.read()).hexdigest()
        return value
