from datetime import datetime
import os
from typing import Optional, TypeVar
from datamanager.dbmanager.db_dir import DB_DIR
from datamanager.registry.db_registry import DB_REGISTRY
from datamanager.filemanager.formats.abstract import ABSTRACT_FORMAT
from datamanager.filemanager.fileparser import PARSER
from datamanager.filemanager.filenames import FILE as BFILE

import logging
logger = logging.getLogger(__name__)

FILE = TypeVar('FILE', bound='BFILE')

class LOADER_DB:
    def __init__( self
                , database_dir         : Optional[DB_DIR]   = None
                , registry             : Optional[str]      = None
                , registry_description : Optional[str]      = None
                , parser               : Optional[PARSER]   = None
                , verbose              : bool               = True ) -> None:
        self.__database_dir   = database_dir if database_dir is not None else None
        self.__verbose        = verbose
        self.__parser         = parser
        self.__local_files    = []
        self.__registry_files : list['FILE'] = []
        self.__record         : dict = None
        self.__registry       = self.__load_registry(filepath=registry, description=registry_description) if registry is not None else None

    def load_db_dir(self, formats: list['ABSTRACT_FORMAT']) -> None:
        """
        Load the database from a directory.
        """
        self.__validate_db_dir()
        self.__load_db_files(formats)
        self.__msg_load_db_report(formats)
        return

    def load_files(self, files: list['FILE']) -> None:
        """
        Load the given files. It checks if the files are in the database directory.
        """
        self.__validate_db_dir()
        self.__local_files = []
        for file in files:
            self.__local_files.append(self.__database_dir.find_file(filename=file.raw))
        return

    def load_from_registry(self, date: str) -> None:
        """
        Load the database from a registry file. By default, load the last registry record.

        Parameters:

            * date : string, the registry record date to load. Format: 'YYYY-MM-DD HH:MM:SS'
        """
        assert isinstance(date, str), f"Given 'date' must be a string, not {type(date)}"
        assert datetime.strptime(date, "%Y-%m-%d %H:%M:%S"), f"Given 'date' must be in the format 'YYYY-MM-DD HH:MM:SS'"
        self.__validate_registry()
        self.__msg(f"Loading database from registry '{self.__registry}' ...")
        self.__load_registry_record(date)
        self.__msg_load_report(date)
        return

    def set_db_directory(self, database_dir: DB_DIR) -> None:
        """
        Set the database directory.
        """
        assert isinstance(database_dir, DB_DIR), f"Given 'database_dir' must be a DB_DIR object, not {type(database_dir)}"
        self.__database_dir = database_dir
        return

    def get_db_directory(self) -> DB_DIR:
        """
        Get the database directory.
        """
        self.__validate_db_dir()
        return self.__database_dir

    def set_registry(self, registry_file: str) -> None:
        """
        Set the registry_file.
        """
        assert isinstance(registry_file, str), f"Given 'registry_file' must be a str object, not {type(registry_file)}"
        self.__registry = self.__load_registry(filepath=registry_file)
        self.__registry = registry_file
        return

    def get_registry_file(self) -> str:
        """
        Get the registry filepath.
        """
        self.__validate_registry()
        return self.__registry

    def get_local_files(self) -> list['FILE']:
        """
        Get the local files.
        """
        self.__validate_db_dir()
        logger.info(f"Number of local files: {len(self.__local_files)}")
        return self.__local_files

    def get_registry_files(self) -> list['FILE']:
        """
        Get the registry files.
        """
        self.__validate_registry()
        return self.__registry_files

    def save_files_as_record(self, description: str, files: list['FILE']) -> None:
        """
        Save the files as a record in the registry.
        """
        self.__validate_registry()
        if len(files) == 0:
            raise ValueError(f"[ERROR]  : There is no files to save in the registry.")
        self.__registry.save_record(self.__registry.create_new_record(description=description, files=files))
        return

    def save_record_of_db(self, description: str) -> None:
        """
        Create a new record in the registry of the local files in the database directory.
        """
        try:
            self.save_files_as_record(description=description, files=self.get_local_files())
        except:
            raise ValueError(f"[ERROR]  : There is no files to save in the registry. Please, try to load if there is any file with selected format first.")
        return

    def dates(self) -> list[str]:
        """
        Get the dates of the registry records.
        """
        self.__validate_registry()
        return self.__registry.dates()

    def get_summary(self) -> dict:
        """
        Get a summary of the database.

        Returns: dict, like { id: {'files': list[FILE], 'fields': set[str], 'formats': set[str]} }
        """
        self.__validate_db_dir()
        return self.__database_dir.get_summary()

    def get_summary_of(self, id: str) -> dict:
        """
        Get a summary of the .

        Parameters:

        * id : str. The id to get the summary.

        Returns: dict, like {'files': list[FILE], 'content_types': set[str], 'formats': set[str]}
        """
        assert isinstance(id, str), f"Given 'id' must be a string, not {type(id)}"
        return self.__database_dir.get_summary_of(id)

    def print_registry(self) -> str:
        """
        Print the registry.
        """
        return f"{self.__registry}"

    def verified_files(self, files: list['FILE']) -> list['FILE']:
        """
        Get the verified files.
        """
        self.__validate_registry()
        verified = []
        for file in files:
            if self.__is_file_in_registry(file):
                verified.append(file)
        return verified

    # Private methods
    def __is_file_in_registry(self, file: 'FILE') -> bool:
        """
        Verify if the file is in the registry.
        It first looks for the filename and then the md5 signature.
        """
        found = False
        for registry_file in self.__record.get('files'):
            if self.__same_filename(file=file, filepath=registry_file.get('filepath')):
                found = True
                break
        if found and file.md5_signature != registry_file.get('md5_signature'):
            logger.warning(f"[WARNING]: File '{file.location}' has a different md5 signature ({file.md5_signature}) than the one in the registry '{registry_file.get('md5_signature')}'.")
        return found and file.md5_signature == registry_file.get('md5_signature')

    def __same_filename(self, file: 'FILE', filepath: str) -> bool:
        """
        Indicates if the given FILE object has the same filename as the given filepath.
        """
        return os.path.basename(file.location) == os.path.basename(filepath)

    def __validate_registry(self) -> None:
        """
        Check if the registry file is defined.
        """
        if self.__registry is None:
            raise ValueError(f"[ERROR]  : Registry file is not defined. Please, set the registry file.")
        return

    def __validate_db_dir(self) -> None:
        """
        Check if the database directory is defined.
        """
        if self.__database_dir is None:
            raise ValueError(f"[ERROR]  : Database directory is not defined. Please, set the database directory.")
        return

    def __load_registry(self, filepath: str, description: Optional[str]) -> DB_REGISTRY:
        """
        Load the registry file. If the file does not exist, create a new one.
        """
        assert isinstance(filepath, str), f"Given 'filepath' must be a string, not {type(filepath)}"
        registry  =  DB_REGISTRY( db_root = self.__database_dir.get_root()
                                , parser  = self.__parser
                                , verbose = self.__verbose )
        if not os.path.exists(filepath):
            registry.create(registry_file = filepath, description=description)
        else:
            registry.load(registry_file = filepath)
        return registry

    def __load_registry_record(self, date: str = None) -> None:
        """
        Load the registry record. If the date is not given, load the last record.
        """
        self.__record = self.__registry.lookup_record(date=date) if date is not None else self.__registry.lookup_record(date=self.__registry.last_date())
        self.__registry_files = [self.__parser.parse(file['filepath']) for file in self.__record.get('files')]
        return

    def __msg_load_report(self, date: str) -> None:
        """
        """
        self.__msg(f"Registry file: '{self.__registry.location()}'")
        self.__msg(f"Creation time: '{self.__registry.creation_time()}'")
        self.__msg(f"Modification time: '{self.__registry.modification_time()}'")
        self.__msg(f"Loaded record date: '{date}'")
        self.__msg(f"\tCreation time: {self.__record.get('creation_time')}")
        self.__msg(f"\tNumber of files: {self.__record.get('nb_files')}")
        return

    def __load_db_files(self, formats: list['ABSTRACT_FORMAT']) -> None:
        """
        Load the database files from the directory.
        """
        self.__local_files = self.__database_dir.find(formats)
        return

    def __msg_load_db_report(self, formats: list['ABSTRACT_FORMAT']) -> None:
        """
        """
        self.__msg(f"Database directory: '{self.__database_dir.get_root()}'")
        self.__msg(f"Looked formats: '{formats}'")
        self.__msg(f"Number of local files: {len(self.__local_files)}")
        return

    def __msg(self, msg) -> None:
        """
        """
        if self.__verbose:
            logger.info(f"{msg}")
        return