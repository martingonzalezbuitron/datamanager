from abc import ABC, abstractmethod
from enum import Enum
import os
from typing import Optional, TypeVar

from datamanager.filemanager.formats.abstract import ABSTRACT_FORMAT
from datamanager.filemanager.filenames import FILE as BFILE

import logging
logger = logging.getLogger(__name__)

FILE = TypeVar('FILE', bound='BFILE')


class OUTPUT_DIR(object):
    """
    Class to handle an output directory for saving files.
    If not defined, the current working directory is used.

    Parameters:

    * output_dir : str. The output directory to be used to save files. (Default: None)
    * verbose    : bool. If set to True, active verbosity. (Default: True)

    Returns: nothing
    """
    def __init__(self, output_dir: Optional[str] = None, alias: Optional[str] = None, verbose: bool = True) -> None:
        self.__output_dir = output_dir if output_dir is not None else f"{os.getcwd()}/"
        self.__alias      = f"{alias} " if alias is not None else ""
        self.__verbose    = verbose
        self.validate_dir()

    def validate_dir(self) -> None:
        """
        Check if the output directory exists. If not, raise an error.
        """
        self.__output_dir = os.path.abspath(self.__output_dir)
        self.__output_dir = f'{self.__output_dir}/' if not self.__output_dir.endswith("/") else self.__output_dir
        try:
            os.access(self.__output_dir, os.F_OK)
        except Exception as e:
            raise ValueError(f"[ERROR]  : Given {self.__alias}directory '{self.__output_dir}' does not exist.")
        else:
            if self.__verbose:
                logger.info(f"Using {self.__alias}directory '{self.__output_dir}'")
        return

    def get_value(self) -> str:
        """
        Get the output directory. If not defined, return the current working directory.
        """
        return self.__output_dir

    @staticmethod
    def create_dir(aDir: str) -> None:
        """
        Create a directory if does not exists.
        """
        create_dir(aDir= aDir)


class DB_DIR(ABC):
    def __init__( self
                , root_dir       : str
                , status         : Enum
                , tree_hierarchy : bool
                , db_name        : Optional[str] = None
                , verbose        : bool = True ) -> None:
        self._output_dir      = OUTPUT_DIR(output_dir= root_dir, alias = db_name, verbose= verbose)
        self._verbose         = verbose
        self._name            = db_name
        self.__status         = status
        self._tree_hierarchy = tree_hierarchy

    def has_tree_hierarchy(self) -> bool:
        return self._tree_hierarchy

    @abstractmethod
    def get_root(self) -> str:
        """
        Get the root directory. If not defined, return the current working directory.
        """
        return self._output_dir.get_value()

    @abstractmethod
    def get_status_dir(self) -> str:
        """
        Get the status directory of the root directory.
        """
        return f"{self.get_root()}{self.__status.value}" if not self.get_root().endswith(self.__status.value) else f"{self.get_root()}"

    @abstractmethod
    def find(self, formats: list['ABSTRACT_FORMAT']) -> list['str'] | list['FILE']:
        """
        Find the files from the local database.

        Parameters:

        * formats : list[ABSTRACT_FORMAT]. A list of formats to be loaded.

        Returns: list['str'] or list['FILE']
        """
        pass

    @abstractmethod
    def find_file(self, filename: str) -> 'FILE':
        """
        Find a file by its filename.

        Parameters:

        * filename : str. The filename to be found.

        Returns: FILE
        """
        pass

    @abstractmethod
    def get_summary(self) -> dict:
        """
        Get a summary of the database.

        Returns: dict, like { id: {'files': list[FILE], 'fields': set[str], 'formats': set[str]} }
        """
        pass

    @abstractmethod
    def get_summary_of(self, id: str) -> dict:
        """
        Get a summary of the id.

        Parameters:

        * id : str. The id to get the summary.

        Returns: dict, like {'files': list[FILE], 'content_types': set[str], 'formats': set[str]}
        """
        pass

    @abstractmethod
    def get_node_dir(self, id: str) -> str:
        """
        Get the directory of a specific id.
        """
        pass

    # A template method
    def create_node_dir(self, id: str) -> None:
        """
        Create a directory for a specific id.
        """
        try:
            self.__new_node_dir(id)
        except Exception as e:
            raise ValueError(f"[ERROR]  : Could not create the node directory for '{id}'")

    @staticmethod
    def create_dir(aDir: str) -> None:
        """
        Create a directory if does not exists.
        """
        OUTPUT_DIR.create_dir(aDir= aDir)

    # Private methods
    # A template method
    def __new_node_dir(self, id: str) -> None:
        """
        Create a new node directory.
        """
        node = self._build_node_dir(id)
        try:
            self.create_dir(node)
        except Exception as e:
            raise e
        return

    @abstractmethod
    def _build_node_dir(self, id: str) -> str:
        """
        Build the node directory.
        """
        pass

# General use functions
def create_dir(aDir: str) -> None:
    """
    Create a folder "aDir" if does not exists.

    Parameters:

    * aDir. string. A valid directory

    Returns: nothing
    """
    if not os.access(aDir, os.F_OK):
        try:
            os.makedirs(aDir)
        except OSError:
            raise OSError(f'[ERROR]  : could not create folder in \'{aDir}\'')
    return